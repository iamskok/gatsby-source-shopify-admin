# Gatsby Source Shopify Admin - WORK IN PROGRESS

Credit to [AngeloAshmore](https://github.com/angeloashmore/gatsby-source-shopify) for writing the original "gatsby-source-shopify" plugin, unfortunately Shopify hides metafields and other information behind their Admin API so I wasn't able to keep using that source plugin.
